#define pin1 2
#define pin2 4
unsigned long delay1 = 700; 
unsigned long delay2 = 800;
unsigned int timeNow;
byte state1 = LOW;
byte state2 = LOW;


void setup() {
  pinMode(2, OUTPUT);
  pinMode(4, OUTPUT);
}

void loop() {
  unsigned long timeNow = millis();

  if ( ( timeNow%1000 > delay1 && ( ((timeNow-timeNow%1000)/2)%1000 == 0) ) || ( (timeNow%1000 < delay1) && ( ((timeNow-timeNow%1000)/2)%1000 == 500)) ){
    state1 = HIGH;
  }
  else{
    state1 = LOW;
  }
  digitalWrite(pin1, state1);

  if ( ( timeNow%1000 > delay2 && ( ((timeNow-timeNow%1000)/2)%1000 == 0) ) || ( (timeNow%1000 < delay2) && ( ((timeNow-timeNow%1000)/2)%1000 == 500)) ){
    state2 = HIGH;
  }
  else{
    state2 = LOW;
  }
  digitalWrite(pin2, state2);

}
