#include <math.h>
#include "rgb_lcd.h"

#define pinSensor A0 // Pin connected to temperature tensor
#define pinLed 2 // Pin connectected to LED
#define pinPot A1 // Pin connected to Grove Rotatory Angle Sensor
#define pinSensor2 A3 // Pin connected to my temperature tensor

rgb_lcd lcd;
// Declaration of used constants
const int R0 = 100000;
const int B = 4275;
const int T0 = 298.15;
const int B2 = 4600;

void setup() {
  lcd.begin(16, 2);
  lcd.setRGB(0, 0, 250);
  pinMode(pinPot, INPUT);

  Serial.begin(9600);
}
void loop(){
  //Read the values from both sensors
  int V = analogRead(pinSensor);
  int V2 = analogRead(pinSensor2);

  //Calculate the resistances
  float R = (1023.0 / V - 1.0) ;
  R = R * R0;
  float R2 = (1023.0 / V2 - 1.0) ;
  R2 = R2 * R0;

  //Converte the measured resistances into temperature
  float T = log(R/R0)/B + 1.0/T0 ;
  T = 1/T - 273.15;
  float T2 = log(R2/R0)/B2 + 1.0/T0 ;
  T2 = 1/T2 - 273.15;

  //Map the value from the potentiometer in range 20-40
  int value = analogRead(pinPot);
  int tresholdT = map(value, 0, 1023, 20, 40);

  //Print to get values to calibrate the B2 constant
  Serial.print(V);
  Serial.print(":");
  Serial.println(V2);

  // Turn on the LED if the temperature overcome the treshold temperature
  if(T > tresholdT){
    digitalWrite(pinLed, HIGH);
  }
  else {
    digitalWrite(pinLed, LOW);
  }

  //Print on the display the temperature treshold that is setted up by changing the angle on the angle sensor
  lcd.setCursor(0, 0);
  lcd.print("T = ");
  lcd.print(T2);
  lcd.print("C");
  lcd.setCursor(0, 1);
  lcd.print("Treshold T = ");
  lcd.print(tresholdT);
  lcd.print("C.");
  delay(300);
}
