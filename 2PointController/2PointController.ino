#include <math.h>
#include "rgb_lcd.h"

#define pinSensor A0 //Pin connected to the circuit with NTC
#define pinPot A1 // Pin connected to Grove Rotatory Angle Sensor
#define pinFan 6 //Pin connected to the fan

rgb_lcd lcd;
// Declaration of used constants
const int R0 = 100000;
const int T0 = 298.15;
const int B = 5400;
int Htreshold; //Higher treshold
const int Ltreshold = 20; //Lower treshold

void setup() {
  lcd.begin(16, 2);
  lcd.setRGB(0, 200, 50);
  pinMode(pinSensor, INPUT);
  pinMode(pinPot, INPUT);
  pinMode(pinFan, OUTPUT);
  // digitalWrite(pinFan, LOW);

  Serial.begin(9600);
}

void loop() {
 //Read the bit value from the temperature sensor and calculate the temperature
  int V = analogRead(pinSensor);
  float R = (1023.0 / V - 1.0) ;
  R = R * R0;
  float T = log(R/R0)/B + 1.0/T0 ;
  T = 1/T - 273.15;

  //Map the value from the potentiometer in range 20-40
  int value = analogRead(pinPot);
  Htreshold = map(value, 0, 1023, 15, 35);

  //Print the value needed to plot the temperature versus time
  Serial.println(V);

  // Turn on the fan if the temperature overcome the high treshold temperature and turn off the fan if the temperature is lower than the low treshold
  if(T > Htreshold){
    analogWrite(pinFan, 255);
  }
  else if(T < Ltreshold){
    analogWrite(pinFan, 0);
  }
  
  


  //Print on the display the temperature treshold that is setted up by changing the angle on the angle sensor
  lcd.setCursor(0, 0);
  lcd.print("T = ");
  lcd.print(T);
  lcd.print("C");
  lcd.setCursor(0, 1);
  lcd.print("Treshold T = ");
  lcd.print(Htreshold);
  lcd.print("C.");
  delay(300);
}



