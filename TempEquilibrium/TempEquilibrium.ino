#include <math.h>
#include "rgb_lcd.h"

#define pinSensor A0 //Pin connected to the circuit with NTC
#define pinPot A1 // Pin connected to Grove Rotatory Angle Sensor
#define pinFan 6 //Pin connected to the fan
#define pinTachometer 2 //Pin connected to the tachometer

int count = 0;
unsigned long start_time;
int rpm;

rgb_lcd lcd;
// Declaration of used constants
const int R0 = 100000;
const int T0 = 298.15;
const int B = 5400;

void setup() {
  lcd.begin(16, 2);
  lcd.setRGB(0, 200, 50);
  pinMode(pinSensor, INPUT);
  pinMode(pinPot, INPUT);
  pinMode(pinFan, OUTPUT);
  pinMode(pinTachometer, INPUT);
  // digitalWrite(pinFan, LOW);

  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(pinTachometer), counter, RISING);
}

void loop() {
  //Modify the duty cicle of the fan starting from 100% and doing a -10% jump each time
  for(float pwm = 255; pwm>24; pwm-=25.5){

    //Measure 60 times for each duty cicle
    for(int i = 0; i<60; i+=1){ 

      //Read the bit value from the temperature sensor and calculate the temperature
      int V = analogRead(pinSensor);
      float R = (1023.0 / V - 1.0) ;
      R = R * R0;
      float T = log(R/R0)/B + 1.0/T0 ;
      T = 1/T - 273.15;

      //Set the speed of the fan accordingly
      analogWrite(pinFan, pwm);

      //Print the values needed for the plot on the serial port
      Serial.print(V);
      Serial.print(":");
      int RPM = RPMcalculator();
      delay(1000);

      //Show on the LED display the actual temperature and the speed of the fan
      lcd.setCursor(0, 0);
      lcd.print("T = ");
      lcd.print(T);
      lcd.print("C");
      lcd.setCursor(0, 1);
      lcd.print("Speed = ");
      lcd.print(RPM);
    }
    //Turn off the fan for 10 seconds to allow the temperature to rise again
    analogWrite(pinFan, 0);
    delay(10000);
  }
}

void counter(){
  count++;
}

//Calculate the RPM value every second
int RPMcalculator(){
  start_time = millis();
  count = 0;
  while((millis() - start_time) < 1000){
  }
  rpm = count * 60 / 2;
  Serial.print(rpm);
  Serial.println();
  return rpm;
}



