import numpy as np
import matplotlib.pyplot as plt
from uncertainties import *
from uncertainties.umath import log
from scipy.stats import sem
from scipy.optimize import curve_fit
import scienceplots

#plt.style.use(['science','ieee'])

# Open the file with data£
with open('Files_txt/Task20.txt', 'r') as file:
    data = []
    time = []

    # Read every line of the file, splitting using ":" to divide the measured Voltage from time
    for riga in file.read().splitlines():
        val1, val2 = map(float, riga.split(':'))

        data.append(val1)
        time.append(val2)

#Values used to calculate the temperature
R0 = 100000
T0 = 273.15 + 25
B = 5400

#Function that calculates temperature given the measured bit voltage
def temp(V):
    R = (1023/V - 1) * R0
    return 1 / (log(R / R0) / B + 1 / T0) - 273.15

#Transforming data list into an array and vectorize the function to calculate with the array
V = np.array(data, dtype=float)
time = np.array(time, dtype=float)
temp2 = np.vectorize(temp)
T = temp2(V)

#Create an array to plot the tresholds
Htreshold = np.full(len(time), 28)
Ltreshold = np.full(len(time), 23)

#Plot the measured data
plt.plot(time, Htreshold, linestyle = '--', label = "Higher treshold", color = 'red')
plt.plot(time, Ltreshold, linestyle = '--', label = "Lower treshold", color = 'black')
plt.plot(time, T, label = "Measured data", color = 'green')
plt.xlabel("Time [s]")
plt.ylabel("Temperature [°C]")
plt.grid()
plt.legend()
plt.savefig('Images/2pointController.png')
plt.show()