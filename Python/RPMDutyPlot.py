import numpy as np
import matplotlib.pyplot as plt
from uncertainties import *
from uncertainties.umath import log
from scipy.stats import sem
from scipy.stats import chi2
from scipy.optimize import curve_fit

# Open the file with data
with open('Files_txt/Task23.txt', 'r') as file:
    duty = []
    rpm = []

    # Read every line of the file, splitting using ":" to divide the measured RPM from duty cicle percentage
    for riga in file.read().splitlines():
        val1, val2 = map(float, riga.split(':'))

        duty.append(val1)
        rpm.append(val2)

#Transforming data list into an array
duty = np.array(duty, dtype=float)
rpm = np.array(rpm, dtype=float)

#Reshape array to get arrays of the same duty cicle to then calculate the mean
duty = duty.reshape(-1, 11)
rpm = rpm.reshape(-1, 11)

#Calculate the mean and the standard deviation as uncertainty for each duty cycle
mean_duty = np.mean(duty, axis=1)
mean_rpm = np.mean(rpm, axis=1)
u_rpm = sem(rpm, axis=1)


#Linear function model
def f(t, m, q):
    return m*t + q
#Curve fitting
initialGuess = np.array([24, 0])
a, b = curve_fit(f, mean_duty, mean_rpm, initialGuess)
print(a, b)

#Chi squared test
chi = np.sum((mean_rpm - f(mean_duty, a[0], a[1])) **2 / f(mean_duty, a[0], a[1]) )
p_value = 1 - chi2.cdf(chi, len(mean_duty)-2)
print("Chi^2 = ", chi, '\n')
print("p value = ", p_value)

#Plot the measured data
plt.errorbar(mean_duty, mean_rpm, yerr = u_rpm, marker = 'o', markersize=2, linestyle = ' ', label = "Measured data")
plt.plot(mean_duty, f(mean_duty, a[0], a[1]))
plt.grid()
plt.xlabel("Duty cicle [%]")
plt.ylabel("RPM [1/min]")
plt.legend()
plt.savefig('Images/RPMDuty.png')
plt.show()