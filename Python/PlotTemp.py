import numpy as np
import matplotlib.pyplot as plt
from uncertainties import *
from uncertainties import unumpy as unp
from uncertainties.umath import log
from scipy.stats import sem
from scipy.stats import chi2
from scipy.optimize import curve_fit

# Open the file with data
with open('Files_txt/Task17.txt', 'r') as file:
    data = []
    time = []

    # Read every line of the file, splitting using ":" to divide the measured Voltage from time
    for riga in file.read().splitlines():
        val1, val2 = map(float, riga.split(':'))

        data.append(val1)
        time.append(val2)

#Values used to calculate the temperature
R0 = ufloat(100000, 1000)
T0 = 273.15 + 25
B = ufloat(5400, 2200)

#Function that calculates temperature given the measured bit voltage
def temp(V):
    R = (1023/V - 1) * R0
    return 1 / (log(R / R0) / B + 1 / T0) - 273.15

#Transforming data list into an array and vectorize the function to calculate with the array
Va = np.array(data, dtype=float)
uV = np.full(len(Va), 3)
V = unp.uarray(Va, uV)
time = np.array(time, dtype=float)
temp2 = np.vectorize(temp)
T = temp2(V)

#Define the model function of the curve fit
def f(t, A, B, C):
    return A*np.exp(B*t) + C

#Fit the data
InitialGuess = np.array([-10., -0.04, 30])
a, b = curve_fit(f, time, unp.nominal_values(T), InitialGuess, sigma = unp.std_devs(T))
print("Values of the fit:", '\n')
print("A = ", a[0], "+/-", np.diag(b)[0], '\n')
print("b = ", a[1], "+/-", np.diag(b)[1], '\n')
print("c = ", a[2], "+/-", np.diag(b)[2], '\n')

#Chi squared test
chi = np.sum((unp.nominal_values(T) - f(time, a[0], a[1], a[2])) **2 / f(time, a[0], a[1], a[2]) )
p_value = 1 - chi2.cdf(chi, len(unp.nominal_values(T))-3)
print("Chi^2 = ", chi, '\n')
print("p value = ", p_value)

#Plot the measured data
plt.plot(time, f(time, *a), label = "Curve fit")
plt.plot(time, unp.nominal_values(T), label = "Measured data", linewidth = 0.5)

plt.xlabel("Time [s]")
plt.ylabel("Temperature [°C]")
plt.grid()
plt.legend()
plt.savefig('Images/CurveFitTemp.png')
plt.show()