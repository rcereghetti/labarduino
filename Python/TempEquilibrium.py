import numpy as np
import matplotlib.pyplot as plt
from uncertainties import *
from uncertainties.umath import log
from scipy.stats import sem
from scipy.optimize import curve_fit
import scienceplots

plt.style.use(['science','ieee'])

# Open the file with data
with open('Files_txt/Task24.txt', 'r') as file:
    V = []
    RPM = []
    time = []

    # Read every line of the file, splitting using ":" to divide the measured Voltage from RPM and from time
    for riga in file.read().splitlines():
        val1, val2, val3 = map(float, riga.split(':'))

        V.append(val1)
        RPM.append(val2)
        time.append(val3)

#Transforming data list into an array
V = np.array(V, dtype=float)
RPM = np.array(RPM, dtype=float)
time = np.array(time, dtype=float)

#Constant used to calculate the temperature
R0 = 100000
T0 = 273.15 + 25
B = 5400

#Function that calculates temperature given the measured bit voltage
def temp(V):
    R = (1023/V - 1) * R0
    return 1 / (log(R / R0) / B + 1 / T0) - 273.15

#Transforming data list into an array and vectorize the function to calculate with the array
V = np.array(V, dtype=float)
RPM = np.array(RPM, dtype=float)
time = np.array(time, dtype=float)
temp2 = np.vectorize(temp)
T = temp2(V)

# Define the number of values to exclude and include (first values are not at equilibrium temperature,
# because the temperature is still converging, so we need to exclude them)
exclude_count = 20
include_count = 40
total = exclude_count + include_count

#Reshape to exclude
reshaped_T = T.reshape(-1, total)
reshaped_RPM = RPM.reshape(-1, total)


mean_T = []
mean_RPM = []
new_time = []

# Iterate over the array with a step size of exclude_count + include_count to exclude the first values
for array in reshaped_T:
    # Exclude the first 20 values and include the next 40 values
    mean_T.append(array[exclude_count:exclude_count + include_count])
for array in reshaped_RPM:
    # Exclude the first 20 values and include the next 40 values
    mean_RPM.append(array[exclude_count:exclude_count + include_count])

for i in range(0, len(time), exclude_count + include_count):
    # Exclude the first two values and include the next two values
    new_time.extend(time[i + exclude_count:i + exclude_count + include_count])

#Calculate the mean value from the last measured values
u_T = sem(mean_T, axis=1)
u_RPM = sem(mean_RPM, axis=1)
mean_T = np.mean(mean_T, axis=1)
mean_RPM = np.mean(mean_RPM, axis=1)
#Plot the measured data
plt.errorbar(mean_RPM, mean_T, yerr = u_T, xerr = u_RPM,fmt = '-o', label = "Measured data", linewidth=1, markersize=1, capsize=2, color='darkred')
plt.xlabel("RPM [1/min]")
plt.ylabel("Equilibrium T [°C]")
plt.grid()
plt.legend()
plt.savefig('Images/TempEquilibrium.png')
plt.show()