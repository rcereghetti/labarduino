import numpy as np
import matplotlib.pyplot as plt
from uncertainties import *
from uncertainties.umath import log
from serial import Serial
from scipy.stats import sem

#All the values to calculate the temperature measured with the Grove Sensor
B = 4275
R0 = 100000
T0 = 273.15 + 25
V = 458

#Calculate T
R = (1023/V - 1) * R0
T = 1 / (log(R / R0) / B + 1 / T0) - 273.15

#Add the uncertainty on the temperature accordingly to the data sheet
T = ufloat(T, 1.5)
print("T of Grove Sensor = ", T)

#------------------------------------------------------------------------------------------------------

#Measured value with observed uncertainty (my sensor)
V2 = ufloat(444, 3)

#Declare new 'constants' to calculate the T of my sensor with uncertainty
R0vd = ufloat(100000, 5000)
R0tr = ufloat(100000, 1000)
B2 = ufloat(4600, 138)

#Calculate the temperature measured with my sensor
R2 = (1023/V2 - 1) * R0vd
T2 = 1 / (log(R2 / R0tr) / B2 + 1 / T0) - 273.15
print("T of my sensor = ", T2)

#Calculate B needed to calibrate my sensor
T = T + 273.15
B2 = log(R2/R0tr)*T*T0 / (T0 - T)
print("New B2 = ", B2)

#Calculate the temperature using the new B
T2 = log(R2/R0tr) / B2 + 1.0/T0
T2 = 1/T2 - 273.15
print("New T of my sensor = ", T2)
