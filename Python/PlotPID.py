import numpy as np
import matplotlib.pyplot as plt
from uncertainties import *
from uncertainties.umath import log
from scipy.stats import sem
from scipy.optimize import curve_fit

# Open the file with data
with open('Files_txt/Task25_17_3_8.txt', 'r') as file:
    V = []
    pwm = []
    time = []

    # Read every line of the file, splitting using ":" to divide the measured Voltage from time
    for riga in file.read().splitlines():
        val1, val2, val3 = map(float, riga.split(':'))

        V.append(val1)
        pwm.append(val2)
        time.append(val3)

#Constant used to calculate the temperature
R0 = 100000
T0 = 273.15 + 25
B = 5400

#Function that calculates temperature given the measured bit voltage
def temp(V):
    R = (1023/V - 1) * R0
    return 1 / (log(R / R0) / B + 1 / T0) - 273.15

#Transforming data list into an array and vectorize the function to calculate with the array
V = np.array(V, dtype=float)
pwm = np.array(pwm, dtype=float)
time = np.array(time, dtype=float)
temp2 = np.vectorize(temp)
T = temp2(V)


#Create an array to plot the treshold
treshold = np.full(len(time), 25)

#Plot the measured data
plt.plot(time, treshold, linewidth = 1, linestyle = '--', label = "Converging temperature (25°C)")
plt.plot(time, T, '-o', markersize = 2, linewidth = 1, label = "Measured data", color='orangered')
plt.xlabel("Time [s]")
plt.ylabel("Temperature [°C]")
plt.title("P = 17       I = 3       D = 8")
plt.legend()
plt.grid()
plt.savefig('Images/PID17_3_8.png')
plt.show()
