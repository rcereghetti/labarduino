#include <math.h>
#include "rgb_lcd.h"
#include <PID_v1.h>

#define pinSensor A0 //Pin connected to the circuit with NTC
#define pinFan 6 //Pin connected to the fan
#define pinTachometer 2 //Pin connected to the tachometer
#define pinPot A1
int count = 0;
unsigned long  start_time;
int rpm, V;
double T, PMW, th, P=25, I=5, D=15; //Choose the PID values

//Initialise PID
PID myPID(&T, &PMW, &th, P, I, D, REVERSE);

rgb_lcd lcd;
// Declaration of used constants
const int R0 = 100000;
const int T0 = 298.15;
const int B = 5400;

void setup() {
  lcd.begin(16, 2);
  lcd.setRGB(0, 200, 50);
  pinMode(pinSensor, INPUT);
  pinMode(pinFan, OUTPUT);
  pinMode(pinTachometer, INPUT);
  Serial.begin(9600);

  //set PID settings
  myPID.SetOutputLimits(0 ,255); 
  myPID.SetMode(AUTOMATIC);
}

void loop() {
  //Read the bit value from the temperature sensor and calculate the temperature
  V = analogRead(pinSensor);
  T = read_temperature();

  //Set the converging temperature using the potentiometer
  th = analogRead(pinPot);
  th = map(th, 0, 1023, 20, 40);

  //Compute PID calculations and set the speed of the fan
  myPID.Compute();
  analogWrite(pinFan, PMW);

  //Print the measured values on the serial port
  Serial.print(V);
  Serial.print(":");
  Serial.println(PMW);

  //Print on the display the actual temperature and the higher treshold
  lcd.setCursor(0, 0);
  lcd.print("T = ");
  lcd.print(T);
  lcd.print("C");
  lcd.setCursor(0, 1);
  lcd.print("Th = ");
  lcd.print(th);
  lcd.print("C");
  delay(200);
}

//Calculate the temperature from the pin value V
float read_temperature(){
  float R = (1023.0 / V - 1.0) ;
  R = R * R0;
  T = log(R/R0)/B + 1.0/T0 ;
  return 1/T - 273.15;
}